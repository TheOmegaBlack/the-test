import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import { MainApp } from './components/MainApp';
import { Footer } from './components/Footer';
import { GlobalStyles } from './style';
import { ContextProvider } from './common/context';

const AppContainer = styled.div`
  min-height: 100vh;
`

function App() {
  return (
    <ContextProvider>
      <GlobalStyles />
      <BrowserRouter>
        <AppContainer>
          <Switch>
            {/*<Route path="/" exact>*/}
            {/*   <LoginPage />*/}
            {/*</Route>*/}
            <Route path="/app">
                <MainApp />
            </Route>
            <Route path="/" exact>
              <Redirect to="/app" />
            </Route>
          </Switch>
          <Footer />
        </AppContainer>
      </BrowserRouter>
    </ContextProvider>
  );
}

export default App;
