import { normalize } from 'styled-normalize';
import { createGlobalStyle } from 'styled-components';
import colors from './colors';

const GlobalStyles = createGlobalStyle`
  @font-face {font-family: "Untitled Sans"; 
  src: url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.eot"); 
  src: url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.eot?#iefix") format("embedded-opentype"), 
  url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.woff2") format("woff2"), 
  url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.woff") format("woff"), 
  url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.ttf") format("truetype"), 
  url("//db.onlinewebfonts.com/t/1a3681e2cd615bd458b68dce439102a4.svg#Copyright Klim Type Foundry") format("svg"); 
  }
  
  ${normalize}
  
  :root {
    --layout-width: 781px;

    ${colors}
  }

  html {
    
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: var(--colors-blackPearl);
    font-family: "Untitled Sans", sans-serif;
    box-sizing: border-box;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  body {
    min-height: 100vh;
  }
`;

export default GlobalStyles;
