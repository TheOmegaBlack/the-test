import { css } from 'styled-components';

const colors = css`
  --colors-alabaster: #F2F3F4;
  --colors-blackPearl: #031323;
  --colors-black: #000000;
`;

export default colors;