import React from 'react';
import { useFetch, useLocalStorage } from '../hooks';

type FeedbackType = {
  [userId: string]: {
    isComplete: boolean
    answers: string[]
  }
}

export type ContextType = {
  users: any[]
  questions: any[]
  feedback: FeedbackType
  setFeedback: (elem: unknown) => void
}

export const AppContext = React.createContext<ContextType>({ users: [], questions: [], feedback: {}, setFeedback: () => {} });

export const ContextProvider = (props: any) => {
  const {
    data: usersData,
    isIdle: usersIdle,
    isError: usersError,
    execute: fetchUsers,
  } = useFetch([]);
  const {
    data: questionsData,
    isIdle: questionsIdle,
    isError: questionsError,
    execute: fetchQuestions,
  } = useFetch([]);

  React.useEffect(() => {
    if (usersError || usersIdle) {
      fetchUsers(
        'https://frontend-exercise-api.netlify.app/.netlify/functions/server/users',
      );
    }
  }, [usersError, usersIdle]);

  React.useEffect(() => {
    if (questionsError || questionsIdle) {
      fetchQuestions(
        'https://frontend-exercise-api.netlify.app/.netlify/functions/server/users',
      );
    }
  }, [questionsError, questionsIdle]);

  const [feedback, setFeedback] = useLocalStorage<FeedbackType>('honesto-12345', {})

  const value = {
    feedback,
    setFeedback,
    users: usersData,
    questions: questionsData,
  };

  return <AppContext.Provider value={value} {...props} />;
};