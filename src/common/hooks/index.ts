import * as React from 'react'

const statusEnum = {
  idle: 'IDLE',
  pending: 'PENDING',
  resolved: 'RESOLVED',
  rejected: 'REJECTED',
}

export const useAsyncAction = <Data>(initialState: Data) => {
  const [status, setStatus] = React.useState(statusEnum.idle)
  const [data, setData] = React.useState(initialState)
  const [error, setError] = React.useState<null | Error>(null)

  const execute = React.useCallback((promise: Promise<Data>): Promise<Error | Data> => {
    setStatus(statusEnum.pending)
    return promise
      .then(
        (data: Data) => {
          setData(data)
          setStatus(statusEnum.resolved)
          return data
        }
      )
      .catch(
        (error: Error) => {
          setError(error)
          setStatus(statusEnum.rejected)
          return error
        },
      )
  }, [])

  return {
    execute,
    data,
    error,
    isIdle: status === statusEnum.idle,
    isLoading: status === statusEnum.pending,
    isSuccess: status === statusEnum.resolved,
    isError: status === statusEnum.rejected,
  }
}

export const useFetch = <Data>(initialState: Data) => {
  const { execute: ex, ...rest } = useAsyncAction<Data>(initialState)

  const execute = React.useCallback(
    (url: Request | string, options: RequestInit | undefined = {}) => ex(
      fetch(url, options)
        .then((res) => {
          if (res.status >= 400) {
            throw new Error('There was a problem')
          }
          return res.json()
        })
        .catch(
          e => {
            throw new Error(e);
          }
        ),
    ),
    [ex],
  )

  return {
    execute,
    ...rest,
  }
}

// taken from https://usehooks.com/useLocalStorage/
export const useLocalStorage = <T>(key: string, initialValue: T) => {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = React.useState<T>(() => {
    try {
      // Get from local storage by key
      const item = window.localStorage.getItem(key);
      // Parse stored json or if none return initialValue
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      // If error also return initialValue
      console.log(error);
      return initialValue;
    }
  });

  // Return a wrapped version of useState's setter function that ...
  // ... persists the new value to localStorage.
  const setValue = (value: T | ((val: T) => T)) => {
    try {
      // Allow value to be a function so we have same API as useState
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      // Save state
      setStoredValue(valueToStore);
      // Save to local storage
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  };

  return [storedValue, setValue];
}
