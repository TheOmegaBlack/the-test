import styled from 'styled-components';
// @ts-ignore
import { ReactComponent as TheoremLogo } from '../../common/svg/TheoremLogo.svg';

export const FooterWrapper = styled.footer`
  position: absolute;
  background-color: var(--colors-blackPearl);
  color: white;
  display: flex;
  font-size: 12px;
  justify-content: space-between;
  bottom: 0;
  height: 54px;
  padding: 19px 11px 19px 132px;
  width: 100%;
  
  .bold {
    font-weight: 600;
  }
`

const Footer = () =>
  <FooterWrapper>
    <TheoremLogo />
    <span>Copyright © 2018 <span className="bold">Theorem</span>, LLC. All Rights Reserved.</span>
  </FooterWrapper>

export default Footer