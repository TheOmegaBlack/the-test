import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import { Header } from '../Header';
import styled from 'styled-components';
import { AppContext } from '../../common/context';
import { ShareFeedback } from '../ShareFeedback';

const Main = styled.main`
  padding-bottom: 54px;
`;

const MainApp = () => {
  const { users, questions, feedback, setFeedback } = React.useContext(AppContext)

  return (
    <>
      <Header />
      <Main>
        <Switch>
          <Route path="/" exact>
            <ShareFeedback />
          </Route>
        </Switch>
      </Main>
    </>
  );
};

export default MainApp;
