import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  background-color: var(--colors-alabaster);
  display: flex;
  justify-content: space-between;
  height: 75px;
  padding-left: 130px;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);

  a.honesto-logo {
    color: var(--colors-black);
    font-size: 24px;
    font-weight: 600;
    margin-top: 22px;
    margin-right: 106px;
    text-decoration: none;
  }
`;

export const UserSection = styled.div`
  border-left: 1px solid #D9DCDE;
  display: flex;
  padding-left: 20px;
  flex: 0 0 241px;
  
  img {
    height: 58px;
    margin-right: 15px;
    margin-top: 9px;
    width: 58px;
  }
  
  div {
    margin-top: 19px;
    
    span {
      display: block;
      
      &.logout {
        color: #59636E;
        font-size: 12px;
        line-height: 14px;
        letter-spacing: 0.15em;
        text-transform: uppercase;
      }
    }
  }
`;

export const CycleWrapper = styled.div`
  margin-right: 20px;
  
  span:first-child {
    color: #59636E;
    display: block;
    font-size: 11px;
    line-height: 13px;
    text-align: right;
    margin-top: 20px;
    margin-bottom: 5px;
  }
  
  .feedback-month {
    font-weight: 600;
  }
  
  .feedback-days {
    color: #19BE9F;
    font-weight: 600;
  }
`;

export const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  width: 100%;

  ul {
    display: flex;
    justify-content: space-between;
    list-style: none;
    padding: 0;
    margin: 0 100px 0 0;
    flex: 1 0 auto;

    a {
      align-items: center;
      color: var(--colors-blackPearl);
      display: flex;
      height: 100%;

      text-decoration: none;
    }
  }
`;

export const activeStyle = {
  borderBottom: '3px solid #AB61E5',
  paddingTop: 3,
};