import { Link, NavLink } from 'react-router-dom';
// @ts-ignore
import UserImage from '../../common/images/user.png';
import { activeStyle, CycleWrapper, HeaderWrapper, Nav, UserSection } from './styles';

export const Header = () => {
  return (
    <HeaderWrapper>
      <Link to="/" className="honesto-logo">
        Honesto
      </Link>
      <Nav>
        <ul>
          <li>
            <NavLink to="/app" activeStyle={activeStyle} exact>
              Share Feedback
            </NavLink>
          </li>
          <li>
            <NavLink to="/app/feedback" activeStyle={activeStyle}>
              My Feedback
            </NavLink>
          </li>
          <li>
            <NavLink to="/app/team-feedback" activeStyle={activeStyle}>
              Team Feedback
            </NavLink>
          </li>
          <li>
            <NavLink to="/app/teams" activeStyle={activeStyle}>
              Teams
            </NavLink>
          </li>
        </ul>
        <CycleWrapper>
          <span>Next Feedback Cycle:</span>
          <span className="feedback-month">Sept 2018</span>
          <span> - </span>
          <span className="feedback-days">4 days</span>
        </CycleWrapper>
      </Nav>
      <UserSection>
        <img src={UserImage} alt="user image" />
        <div>
          <span className="name">Jane Smith</span>
          <span className="logout">Logout</span>
        </div>
      </UserSection>
    </HeaderWrapper>
  );
};

export default Header;
