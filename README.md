# Theorem Test

## How to run

TBD

## Phase 1 - Planning/Scoping

I started by analyzing the page, trying to scope it down to something that sounds reasonable to build in a few hours.
By analyzing the page structure, I broke it down into a few elements. There's a footer that appears on all pages,
a header that appears after logging into the app. Since I'm imagining other pages like login, existing outside the main app,
I split the application this way

```javascript

<App>
  <Router>
    <Switch>
      <Login />
      <MainApp>
        <Switch>
          <ShareFeedback />
          <MyFeedback />
        </Switch>
      </MainApp>
    </Switch>
  </Router>
  <Footer />
</App>

```

I'm leaving the Login page for last, if I spare enough time to build it.

State of application: Context + Localstorage

Since there is just one big load upfront, I decided to use the context api instead of something more complex like Redux.
I have implemented a very simple retry that takes care of the 500 error.
Inside the custom hook for context, I am using another custom hook I created and have been using in several of my projects, useFetch.

Local storage is used for all the "persistent data".

## Phase 2 - Building!

I started first by creating a draft of the rooting, and then proceeded to create each element in order of 
Footer, since it is the element that appears on all routes.
